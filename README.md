# Publish metadata in Beyond Geonetwork
## Prerequisites and Installation
TODO
## Acknowledgement
+ geonapi : R librairy to insert/update/delete metadata directly in our geonetwork : https://github.com/eblondel/geonapi
+ geometa : R librairy to create iso 19115 xml (inspire compliant) : https://github.com/eblondel/geometa
## License
This code is provided under the [CeCILL-B](https://cecill.info/licences/Licence_CeCILL-B_V1-en.html) free software license agreement.